<?php
use Phalcon\Mvc\Micro;

$app = new Micro();

$app->get(
	"/",
	function () {
		echo "<h1>Microservice up and running!</h1>";
		phpinfo();
	}
);

$app->get(
	"/say/welcome/{name}",
	function ($name) {
		echo "<h1>Welcome $name!</h1>";
	}
);

$app->handle();
