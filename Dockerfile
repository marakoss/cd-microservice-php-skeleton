FROM phusion/baseimage

# Phalcon PHP framework

RUN curl -s https://packagecloud.io/install/repositories/phalcon/stable/script.deb.sh | bash

RUN	\
	apt-get update \
	&&	apt-get -y upgrade \
	&&	apt-get update --fix-missing

# PHP 7

RUN \
	apt-get install -y \
		php7.0 \
		php7.0-bcmath \
		php7.0-cli \
		php7.0-common \
		php7.0-fpm \
		php7.0-gd \
		php7.0-gmp \
		php7.0-intl \
		php7.0-json \
		php7.0-mbstring \
		php7.0-mcrypt \
		php7.0-mysqlnd \
		php7.0-opcache \
		php7.0-pdo \
		php7.0-xml \
		php7.0-zip \
		php7.0-phalcon \
		zip \
		unzip

# Composer

RUN curl -sS https://getcomposer.org/installer | php
RUN mv composer.phar /usr/local/bin/composer

# Start PHP-FPM

RUN service php7.0-fpm start

# Nginx \ Supervisor \ git
RUN \
	apt-get install -y \
		nginx-full \
		supervisor \
		git

# Make
RUN apt-get -y install build-essential

RUN apt-get clean
RUN apt-get autoclean

# Copy settings

COPY docker/.bashrc /root/.bashrc
COPY docker/supervisord.conf /etc/supervisor/conf.d/supervisord.conf
COPY docker/nginx.conf /etc/nginx/sites-enabled/default
#COPY docker/php.ini /etc/php/7.0/fpm/php.ini

# Mounting points

WORKDIR /var/www/service
VOLUME ["/var/www/service/"]
EXPOSE 80 443

# Create log dir
RUN mkdir -p var/logs

# Run nginx & php-fpm with supervisor

CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/conf.d/supervisord.conf"]
