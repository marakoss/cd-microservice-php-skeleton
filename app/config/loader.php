<?php

$loader = new \Phalcon\Loader();

/**
 * We're a registering a set of directories taken from the configuration file
 */
$loader->registerDirs(
	array(
		$config->application->controllersDir,
		$config->application->modelsDir
	)
);

/**
 *  Resgister all vendor libraries using composer generated autoloader.
 */
$loader->registerFiles(
	[
		getcwd() . '/vendor/autoload.php'
	]
);

$loader->register();
